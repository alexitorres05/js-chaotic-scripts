const GenAlgo = require('./GenAlgo');

class Map {
    constructor(w, h) {
        this._w = w;
        this._h = h;
        this._wh = w * h;
        this._map = new Array(this._wh).fill(0);
    }

    getWidth() {
        return this._w;
    }

    getHeight() {
        return this._h;
    }
}

class Character {
    constructor(x, y) {
        this._x = x;
        this._y = y;
    }

    action(map) {
        this._x += GenAlgo.randInt(-5, 5);
        this._y += GenAlgo.randInt(-5, 5);
        this._x = GenAlgo.limitArea(this._x, 0, map.getWidth() - 1);
        this._y = GenAlgo.limitArea(this._y, 0, map.getHeight() - 1);
    }

    getX() {
        return this._x;
    }

    getY() {
        return this._y;
    }
}

(() => {
    const limit = { w: 50, h: 50 };

    /* Map */
    const map = new Map(limit.w, limit.h);

    /* Prepare the simulation */
    const app = new GenAlgo({
        init: {
            nb: 40,
            factory: () =>
                new Character(
                    GenAlgo.randInt(0, limit.w),
                    GenAlgo.randInt(0, limit.h)
                )
        },
        evaluate: {
            time: 200,
            action: obj => {
                obj.action(map);
            },
            fitness: obj => {
                const diff = GenAlgo.distVector2d(
                    [0, 0],
                    [obj.getX(), obj.getY()]
                );
                return diff < 5 ? 3 : diff < 10 ? 2 : 1;
            }
        },
        turn: {
            keep: 10,
            filter: () => {
                //
            },
            combine: () => {
                //
            }
        },
        finish: {
            cycles: 20
        }
    });

    app.run();
})();
