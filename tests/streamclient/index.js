const express = require('express');
const http = require('http');
const path = require('path');
const fs = require('fs');

const RtmpServer = require('rtmp-server');

const HTTP_CONF = { host: '0.0.0.0', port: 3110 };
const RTMP_CONF = { host: '0.0.0.0', port: 1935 };

/* Server Express */

let app = express();
let server = http.Server(app);

app.get(/^\/?test$/, (req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
    res.write(
        fs.readFileSync(path.resolve(__dirname, 'public', 'player.html'))
    );
    res.end();
});

app.get(/^\/?(.+)$/, (req, res) => {
    res.writeHead(200, { 'Content-Type': 'application/json; charset=utf-8' });
    res.write(
        JSON.stringify({
            success: true,
            error: false,
            data: {
                http: `http://${HTTP_CONF.host}:${HTTP_CONF.port}`,
                rtmp: `rtmp://${RTMP_CONF.host}:${RTMP_CONF.port}`,
                publish: '/test'
            }
        })
    );
    res.end();
});

server.listen(HTTP_CONF.port, HTTP_CONF.host, () =>
    console.log(
        `The Web server working on: http://${HTTP_CONF.host}:${HTTP_CONF.port}`
    )
);

/* Streaming Server */

const rtmpServer = new RtmpServer();

rtmpServer.on('client', client => {
    client.on('connect', () => console.log(`[RTMP:CONNECT] ${client.id}`));

    client.on('play', ({ streamName }) => {
        console.log(`[RTMP:PLAY] ${client.id}`);
    });

    client.on('publish', ({ streamName }) => {
        console.log(`[RTMP:PUBLISH] ${client.id}`);
    });

    client.on('stop', () => console.log(`[RTMP:DISCONNECT] ${client.id}`));
});
rtmpServer.on('error', err => console.error(err));

rtmpServer.listen(RTMP_CONF.port, RTMP_CONF.host, () =>
    console.log(
        `Streaming server working on: rtmp://${RTMP_CONF.host}:${RTMP_CONF.port}`
    )
);
