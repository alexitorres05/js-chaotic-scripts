/* https://gitlab.com/Yarflam/pyscripts/-/blob/master/py3/semantic/semantic.py */
const Semantic = require('./Semantic');

(() => {
    const app = new Semantic();
    const tensors = [
        'Serpent d\'argent',
        'Dragon des landes',
        'Ange des landes',
        'Mélomancien du ciel d\'azur',
        'Arachnide des ombres'
    ].map(words => app.embedding(words));
    console.log(tensors);
})();
