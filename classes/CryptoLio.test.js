const CryptoLio = require('./CryptoLio');

(() => {
    const key = 'MyB3stP4ss!';
    const message = 'Bob, are you okay today?';
    const encrypt = CryptoLio.encrypt(message, key);
})();
