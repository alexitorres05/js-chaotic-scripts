class DateTime {
    static ONE_SECOND = 1000;
    static ONE_MINUTE = DateTime.ONE_SECOND * 60;
    static ONE_HOUR = DateTime.ONE_MINUTE * 60;
    static ONE_DAY = DateTime.ONE_HOUR * 24;
    static ONE_WEEK = DateTime.ONE_DAY * 7;

    constructor(d = 'now') {
        this.setDate(d);
    }

    format(chain) {
        Object.values({
            ms_s: ['_s', `00${this._date.getMilliseconds()}`.substr(-3)],
            sec_ss: ['s{2}', `0${this._date.getSeconds()}`.substr(-2)],
            min_ii: ['i{2}', `0${this._date.getMinutes()}`.substr(-2)],
            hour_HH: ['H{2}', `0${this._date.getHours()}`.substr(-2)],
            day_dd: ['d{2}', `0${this._date.getDate()}`.substr(-2)],
            month_mm: ['m{2}', `0${this._date.getMonth() + 1}`.substr(-2)],
            year_yyyy: ['Y{4}', this._date.getFullYear()],
            year_yy: ['Y{2}', String(this._date.getFullYear()).substr(2)],
            year_yy: ['GMT', String(this._date.getTimezoneOffset())]
        }).forEach(([rgx, value]) => {
            chain = chain.replace(
                new RegExp(`(^|[^\\\\])${rgx}`),
                '$1' + value
            );
        });
        /* Return */
        return chain;
    }

    nextDays(n = 1) {
        let ts = this._date.getTime();
        let mul = n >= 0 ? 1 : -1;
        let loop = Math.abs(Math.floor(n));
        while (loop--) {
            ts += mul * DateTime.ONE_DAY;
        }
        this._date = new Date(ts);
        return this;
    }

    clone() {
        const Self = this.constructor;
        return new Self(this._date);
    }

    setDate(d) {
        if (d === null) d = new Date(0);
        if (d === 'now') d = new Date();
        if (typeof d === 'string') d = new Date(d);
        if (typeof d === 'number') d = new Date(Math.floor(d));
        if (isNaN(d)) d = new Date(0);
        this._date = d;
        return this;
    }

    getNative() {
        return this._date;
    }
}

module.exports = DateTime;
