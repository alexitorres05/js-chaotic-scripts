class CryptoLio {
    static encrypt(message, key) {
        key = this._getChrCode(key);
        message = this._getChrCode(message);
        let keyMap = this._getMapAverage(key);
        let msgMap = this._getMapAverage(message);
        console.log({
            keyMap,
            msgMap
        });
    }

    static _getChrCode(str) {
        if (typeof str !== 'string') return [];
        return str.split('').map(x => x.charCodeAt(0));
    }

    static _getMapAverage(arr) {
        let average = this._getAverage(arr);
        let data = arr.map(x => x - average);
        let levels = data.map(x => (x >= 0 ? 1 : -1));
        return {
            av: average,
            lv: levels,
            data: data.map(x => Math.abs(x) + 1)
        };
    }

    static _getAverage(arr) {
        const [a, b] = arr.reduce(
            (accum, x) => {
                /* Sum */
                accum[0] += x;
                accum[1]++;
                /* GCD */
                let factor = this._gcd(...accum) || 1;
                accum[0] /= factor;
                accum[1] /= factor;
                return accum;
            },
            [0, 0]
        );
        return Math.round(a / (b || 1));
    }

    static _gcd(a, b) {
        while (a % b) [a, b] = [b, a % b];
        return b;
    }
}

module.exports = CryptoLio;
