const DateTime = require('./DateTime');

/* Main */
(() => {
    let d = new DateTime('2021-04-03T03:04:05.123Z');
    console.log(d.format('YYYY-mm-ddTHH:ii:ss._sZGMT'));
    d.nextDays(-10);
    console.log(d.format('YYYY-mm-ddTHH:ii:ss._sZGMT'));
})();
