class Dwarf {
    constructor(x=0, y=0, z=0) {
        this._x = x;
        this._y = y;
        this._z = z;
        this._world = null;
    }
    
    next() {}
    
    setWorld(world) {
        this._world = world;
    }
}

class Spawn {
    constructor(Builder) {
        this._builder = Builder;
        this._counter = Spawn.DEFAULT_COUNTER;
        this._world = null;
    }
    
    next() {
        if(!this._counter) {
            this._world.addObj(new this._builder());
            this._counter = Spawn.DEFAULT_COUNTER;
        } else this._counter--;
    }
    
    setWorld(world) {
        this._world = world;
    }
}
Spawn.DEFAULT_COUNTER = 10;

class World {
    constructor() {
        this._obj = [];
    }
    
    next() {
        this._obj.forEach(obj => {
            obj.next();
        });
    }
    
    addObj(obj) {
        this._obj.push(obj);
        obj.setWorld(this);
        return obj;
    }
    
    addSpawn(Builder) {
        return this.addObj(new Spawn(Builder));
    }
}

(() => {
    /* Create a world */
    const world = new World();
    /* Create a Dwarf spawn */
    world.addSpawn(Dwarf);
    /*  */
    world.next();
    console.log(world);
})();
