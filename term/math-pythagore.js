let [a, b] = process.argv.length >= 4 ? process.argv.slice(-2) : [false, false];

(() => {
    if (!a || !b) return false;
    /* Prepare */
    a = a
        .split(',')
        .map(x => Number(x))
        .filter(x => !isNaN(x));
    b = b
        .split(',')
        .map(x => Number(x))
        .filter(x => !isNaN(x));
    if (!(a.length && a.length === b.length)) return false;
    /* Calculate */
    let result = a
        .map((x, i) => [x, b[i]])
        .reduce((accum, [x, y]) => accum + (y - x) * (y - x), 0);
    result = Math.sqrt(Math.abs(result));
    /* Show */
    console.log(result);
})();
