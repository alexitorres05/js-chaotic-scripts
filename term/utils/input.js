let history = {
    index: -1,
    limit: 20,
    items: []
};

const cursor = {
    _write: (n, data) => {
        new Array(n).fill(0).forEach(() => process.stdout.write(data));
        return cursor;
    },
    left: (n = 1) => {
        return cursor._write(n, '\x08');
    },
    space: (n = 1) => {
        return cursor._write(n, '\x20');
    },
    write: data => {
        process.stdout.write(data);
        return cursor;
    }
};

function input(message) {
    return new Promise(resolve => {
        /* TTY mode */
        const isTTY = process.stdin.isTTY;
        if (isTTY) process.stdin.setRawMode(true);
        /* Event */
        let data = '',
            pos = 0;
        const evtCapture = buff => {
            let chunk = buff.toString('utf8');
            let ckCodes = chunk.split('').map(x => x.charCodeAt(0));
            /* TTY: stdout */
            if (isTTY) {
                /* CTRL+C -> quit */
                if (ckCodes[0] === 3) {
                    data = 'quit';
                    chunk = '\r';
                }
                /* Treatment */
                if (chunk !== '\r') {
                    /* History */
                    if (
                        ckCodes[0] == 27 &&
                        ckCodes[1] == 91 &&
                        (ckCodes[2] == 65 || ckCodes[2] == 66)
                    ) {
                        if (!history.items.length) return;
                        /* (1) Move right and eraze */
                        cursor
                            .left(pos)
                            .space(data.length)
                            .left(data.length);
                        /* (2) Move */
                        // TOP
                        if (ckCodes[2] == 65) {
                            history.index = Math.min(
                                history.index + 1,
                                history.items.length - 1
                            );
                            data = history.items[history.index];
                        }
                        // BOTTOM
                        else if (ckCodes[2] == 66) {
                            history.index = Math.max(history.index - 1, -1);
                            if (history.index >= 0) {
                                data = history.items[history.index];
                            } else data = '';
                        }
                        /* (3) Print */
                        cursor.write(data);
                        pos = data.length;
                        return;
                    }
                    /* Move */
                    if (
                        ckCodes[0] == 27 &&
                        ckCodes[1] == 91 &&
                        (ckCodes[2] == 68 || ckCodes[2] == 67)
                    ) {
                        // LEFT
                        if (ckCodes[2] === 68 && pos >= 1) {
                            cursor.left();
                            pos--;
                        }
                        // RIGHT
                        else if (ckCodes[2] === 67 && pos < data.length) {
                            cursor.write(data[pos]);
                            pos++;
                        }
                        return;
                    }
                    /* Space/Tab */
                    if (ckCodes[0] == 9 || ckCodes[0] == 32) {
                        if (!data.length) return;
                        /* Tab */
                        if (ckCodes[0] == 9) {
                            chunk = new Array(4 - (pos % 4)).fill(' ').join('');
                        }
                    }
                    /* Back */
                    if (ckCodes[0] === 127) {
                        if (data.length) {
                            cursor
                                .left()
                                .space(data.length - pos + 1)
                                .left(data.length - pos + 1)
                                .write(data.slice(pos))
                                .left(data.length - pos);
                            data = data.slice(0, pos - 1) + data.slice(pos);
                            pos--;
                        }
                        return;
                    }
                    /* Standard */
                    cursor.write(chunk);
                    if (pos < data.length) {
                        cursor.write(data.slice(pos)).left(data.length - pos);
                        data = data.slice(0, pos) + chunk + data.slice(pos);
                    } else data += chunk;
                    pos += chunk.length;
                    return;
                } else cursor.write('\n');
                data += chunk;
            } else data = chunk;
            /* Close the listener */
            if (isTTY) process.stdin.setRawMode(false);
            process.stdin.removeListener('data', evtCapture);
            process.stdin.pause();
            data = data.slice(0, -1);
            /* Keep save in history */
            if (data.length) {
                history.items = [data]
                    .concat(history.items)
                    .slice(0, history.limit);
            }
            history.index = -1;
            /* Callback */
            resolve(data);
        };
        process.stdin.on('data', evtCapture);
        /* Prompt */
        process.stdout.write(message);
        process.stdin.resume();
    });
}

module.exports = input;
